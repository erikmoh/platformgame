import pygame
from settings import *
from borders import *


class platformX(object):
    def __init__(self, img, x, y):
        # image
        self.imgstr = img
        self.img = pygame.image.load(self.imgstr)
        # position
        self.x = x
        self.y = y

    # draw platform
    def draw(self, screen):
        screen.blit(self.img, (round(self.x), round(self.y)))

    # move platform
    def move(self):
        self.x -= PLATFORMSPEED