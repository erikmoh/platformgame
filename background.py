import pygame
from settings import *


class background(object):
    def __init__(self, img, x, y):
        # image
        self.img = img
        # position
        self.x = x
        self.y = y

    # draw background
    def draw(self, screen):
        screen.blit(self.img, (round(self.x), round(self.y)))

    # move background
    def move(self):
        self.x -= BACKGROUNDSPEED
        if self.x <= -self.img.get_width():
            self.x = self.img.get_width()


# image
backgroundImg = pygame.image.load(BACKGROUNDIMG)

# init
background1 = background(backgroundImg, 0, 0)
background2 = background(backgroundImg, backgroundImg.get_width(), 0)
backgrounds = [background1, background2]    