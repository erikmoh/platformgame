import pygame
from settings import *
from borders import *


class button(object):
    def __init__(self, img, x, y):
        # image
        self.img = img
        # position
        self.x = x
        self.y = y

    # draw player
    def draw(self, screen):
        screen.blit(self.img, (round(self.x), round(self.y)))

    def isClicked(self, mousePos):
        mouseX = mousePos[0]
        mouseY = mousePos[1]
        return (topBorder(self) < mouseY < bottomBorder(self) and 
                leftBorder(self) < mouseX < rightBorder(self))


