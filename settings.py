# tickrate
FPS = 144

# resolution, window size
WIDTH = 1280
HEIGHT = 720

# name
GAMENAME = "Platform Game"

# images
ICONIMG = 'img/player/player.png'
# player
PLAYERIMG = 'img/player/player.png'
PLAYERIMGCLIPPING = 'img/player/playerClipping.png'
# background
BACKGROUNDIMG = 'img/background.png'
# buttons
STARTBTNING = "img/buttons/startBtn.png"
RETRYBTNIMG = "img/buttons/retryBtn.png"
LEVELSBTNIMG = "img/buttons/levelsBtn.png"
NEXTBTNIMG = "img/buttons/nextBtn.png"
# platforms (BCLP = bottom left corner platform, etc)
SP = 'img/platforms/singlePlat.png'
FP = 'img/platforms/floorPlat.png'
LFP = 'img/platforms/leftFloorPlat.png'
RFP = 'img/platforms/rightFloorPlat.png'
WP = 'img/platforms/wallPlat.png'
TWP = 'img/platforms/topWallPlat.png'
BWP = 'img/platforms/bottomWallPlat.png'
TLCP = 'img/platforms/topLeftCornerPlat.png'
TRCP = 'img/platforms/topRightCornerPlat.png'
BLCP = 'img/platforms/bottomLeftCornerPlat.png'
BRCP = 'img/platforms/bottomRightCornerPlat.png'

# gravity
ACCELERATIONRATE = 0.3
VELOCITYINCREMENT = 0.2

# jumping
JUMPPOWER = 7

# clipping
CLIPLENGTH = 15

# platform
PLATFORMSPEED = 3
PLATFORMCOUNT = 0

# background
BACKGROUNDSPEED = 0.2
