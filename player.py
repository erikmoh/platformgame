import pygame
from settings import *
from borders import *


class player(object):
    def __init__(self, img, x, y):
        # image
        self.img = img
        # position
        self.x = x
        self.y = y
        # jumping variables
        self.jumpAvailable = False
        self.velocity = -VELOCITYINCREMENT
        # clipping variables
        self.clippingCount = 0
        self.collide = False
        # other
        self.platformCollisionClipping = 0

    # draw player
    def draw(self, screen):
        screen.blit(self.img, (round(self.x), round(self.y)))

    # set player at start position
    def startPos(self):
        self.x = 100
        self.y = 200
        self.jumpAvailable = True
        self.velocity = -VELOCITYINCREMENT
        self.clippingCount = 0
        self.collide = False
        self.bounceLeft = False
        self.platformCollisionClipping = 0

    # jump
    def jump(self):
        self.velocity = JUMPPOWER
        self.jumpAvailable = False

    # clip
    def clip(self):
        self.clippingCount = CLIPLENGTH
        self.img = playerClippingImg

    # count down clipping timer
    def countdownClipping(self):
        if self.clippingCount > 0:
            if self.platformCollisionClipping <= 0:
                self.clippingCount -= 1
        else:
            self.img = playerImg
    
    # calculate position when accelerating (jumping or falling)
    def calcPosAccelerate(self):
        # upwards momentum
        if self.velocity > 0:
            self.y -= (self.velocity ** 2) * ACCELERATIONRATE
        # downwards momentum
        elif self.velocity > -JUMPPOWER:
            self.y += (self.velocity ** 2) * ACCELERATIONRATE
        # downwards momentum static max velocity
        else: 
            self.y += (JUMPPOWER ** 2) * ACCELERATIONRATE

    # check ceiling boundary
    def ceilingBoundary(self, screen):
        return self.y <= 0

    # check platform collision top and bottom
    def platformCollisionHeight(self, platform):
        if (bottomBorder(self) > topBorder(platform) and topBorder(self) < bottomBorder(platform) and 
        rightBorder(self) > leftBorder(platform) and leftBorder(self) < rightBorder(platform) - 20):
            # if clipping finished during collision
            if self.clippingCount <= 0:
                self.platformCollisionClipping -= 1
                return True
            # if still clipping at collision
            else:
                # number of platforms, becuase it must check all platforms
                self.platformCollisionClipping = PLATFORMCOUNT
                return False
        self.platformCollisionClipping -= 1
        return False

    # check platform collision left
    def platformCollisionLeft(self, platform):
        return (leftBorder(platform) < rightBorder(self) and leftBorder(self) < leftBorder(platform) - (self.img.get_width() - 5) and
                bottomBorder(self) > topBorder(platform) and topBorder(self) < bottomBorder(platform))

    # set player under ceiling
    def setPlayerUnderCeiling(self, screen):
        self.y = 1
        self.velocity = -VELOCITYINCREMENT
        self.bounceLeft = False

    # set player under platform
    def setPlayerUnderPlatform(self, platform):
        self.y = bottomBorder(platform)
        self.velocity = -VELOCITYINCREMENT
        self.bounceLeft = False
    
    # set player on platform
    def setPlayerOnPlatform(self, platform):
        self.y = topBorder(platform) - self.img.get_height()
        self.jumpAvailable = True
        self.velocity = -VELOCITYINCREMENT
        self.bounceLeft = False

    # set player left of platform
    def setPlayerLeftPlatform(self, platform):
        self.x = leftBorder(platform) - self.img.get_width()


playerImg = pygame.image.load(PLAYERIMG)
playerClippingImg = pygame.image.load(PLAYERIMGCLIPPING)

player = player(playerImg, 0, 0)