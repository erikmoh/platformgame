import pygame
from settings import *
from background import *
from player import *
from platforms import *
from button import *
from levels import *


# initialize pygame
pygame.init()
# create screen
screen = pygame.display.set_mode((WIDTH, HEIGHT))
# set font
font = pygame.font.SysFont('Verdana', 30)
# set images
pygame.display.set_caption(GAMENAME)
pygame.display.set_icon(pygame.image.load(ICONIMG))
# set clock
clock = pygame.time.Clock()

# get images
startImg = pygame.image.load(STARTBTNING)
nextImg = pygame.image.load(NEXTBTNIMG)
retryImg = pygame.image.load(RETRYBTNIMG)
levelsImg = pygame.image.load(LEVELSBTNIMG)

# init buttons
startBtn = button(startImg, round((WIDTH/2) - (startImg.get_width()/2)), round(HEIGHT/2) - 100)
nextBtn = button(nextImg, round((WIDTH/2) - (nextImg.get_width()/2)), round(HEIGHT/2) - 100)
retryBtn = button(retryImg, round((WIDTH/2) - (retryImg.get_width()/2)), round(HEIGHT/2) - 100)
levelsBtn = button(levelsImg, round((WIDTH/2) - (levelsImg.get_width()/2)), round(HEIGHT/2))


class game(object):
    def __init__(self, running):
        # running
        self.running = running
        self.currentlevel = 1

    def initPlatforms(self, currentlvlplatforms):
        self.platforms = []
        # change level
        for platform in currentlvlplatforms:
            self.platforms.append(platformX(platform[0], platform[1], platform[2]))
        PLATFORMCOUNT = len(game.platforms)


def checkCloseWindow():
    # check for close window event
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.display.quit()
            pygame.quit()
            exit()


def drawRestartGameWindow(result):
    # fill background
    screen.fill((200, 200, 255))
    # stop game
    game.running = False

    # set mouse visible
    pygame.mouse.set_visible(True) 

    # draw levels button
    levelsBtn.draw(screen)
    
    # check won/loss/start state
    if result == "":
        # draw start button
        startBtn.draw(screen)
    else:
        if result == "won":
            game.currentlevel += 1 
            # draw next button
            nextBtn.draw(screen)
        else:
            # draw retry button
            retryBtn.draw(screen)
        # create result message
        resultStr = "You " + result + "!"
        resultDisplay = font.render(resultStr, False, (0, 0, 0))
        # draw result message
        screen.blit(resultDisplay, (round((WIDTH/2) - resultDisplay.get_width()/2), round((HEIGHT/2) - 200)))
        
    # create level display
    levelStr = "Level: " + str(game.currentlevel)
    levelDisplay = font.render(levelStr, False, (0, 0, 0))
    # draw level display
    screen.blit(levelDisplay, (20, 10))
    
    # update screen
    pygame.display.update()

    # wait for start
    while not game.running:
        # check for close window event
        checkCloseWindow()
        # start
        if pygame.mouse.get_pressed()[0] and startBtn.isClicked(pygame.mouse.get_pos()):
            # reset platforms
            game.initPlatforms(levels[game.currentlevel - 1])
            
            # reset player
            player.startPos()
            player.jumpAvailable = False

            # create alpha overlay
            overlay = pygame.Surface((1280,720))
            overlay.set_alpha(128)
            overlay.fill((200, 200, 255))

            # hide mouse
            pygame.mouse.set_visible(False)            

            # draw countdown text
            for number in range(3, 0, -1):
                # draw start of level
                drawRunningGameWindow()
                # draw overlay
                screen.blit(overlay, (0, 0))
                # create and draw countdown number
                countdownNumber = font.render(str(number), False, (0, 0, 0))
                screen.blit(countdownNumber, (round(WIDTH/2), round(HEIGHT/2)))
                # update screen
                pygame.display.update()
                # wait 1 sec between numbers
                pygame.time.wait(1000)

            game.running = True


def drawRunningGameWindow():
    # fill background
    screen.fill((200, 200, 255))
    for background in backgrounds:
        background.draw(screen)
    
    # draw and position elements
    for platform in game.platforms:
        if -40 < platform.x < WIDTH:
            platform.draw(screen)
    player.draw(screen)




# init game
game = game(False)
# draw start window
drawRestartGameWindow("")

# loop
while game.running:
    
    # clock speed
    clock.tick(FPS)

    # check for close window event
    checkCloseWindow()

    # array of all keys being pressed
    keys = pygame.key.get_pressed()
    
    # jump
    if keys[pygame.K_w] and player.jumpAvailable:
        player.jump()
    # clip
    if keys[pygame.K_s]:
        player.clip()

    # move backgrounds
    for background in backgrounds:
        background.move()
    
    # move platforms
    for platform in game.platforms:
        platform.move()
        if rightBorder(platform) < 0:
            game.platforms.remove(platform)
    if len(game.platforms) < 1:  
        drawRestartGameWindow("won")
    PLATFORMCOUNT = len(game.platforms)

    # clipping countdown
    player.countdownClipping()
    #player.platformCollisionClipping = 0

    # push player to left if collision with platform
    for platform in game.platforms:
        leftPlatforms = [SP, LFP, WP, TWP, BWP, TLCP, BLCP]
        if player.platformCollisionLeft(platform) and platform.imgstr in leftPlatforms:
            player.setPlayerLeftPlatform(platform)
            player.x -= PLATFORMSPEED

    # end game if player is pushed out of the world
    if rightBorder(player) <= 0:
        drawRestartGameWindow("lost")

    # falling
    if player.velocity < 0:
        # calculate new position
        player.calcPosAccelerate()
        # put player on platform if collision with platform
        for platform in game.platforms:
            if player.platformCollisionHeight(platform):
                player.setPlayerOnPlatform(platform)
        # end game if player falls out of world
        if topBorder(player) > screen.get_height():
            drawRestartGameWindow("lost")
        # increment velocity
        player.velocity -= VELOCITYINCREMENT

    # jumping
    elif player.velocity > 0:
        # calculate new position
        player.calcPosAccelerate()
        # put player under platform if collision with platform
        for platform in game.platforms:
            if player.platformCollisionHeight(platform):
                player.setPlayerUnderPlatform(platform)
        # put player under ceiling if collision with screen
        if player.ceilingBoundary(screen):
            player.setPlayerUnderCeiling(screen)
        # increment velocity
        else:
            player.velocity -= VELOCITYINCREMENT

    # refresh page with new positions and values
    drawRunningGameWindow()
    # update display
    pygame.display.update()
