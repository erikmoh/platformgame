import pygame
from settings import *

# tile dimensions 32x18

def addSinglePlatform(level, startCoordinates):
    level.append((SP, startCoordinates[0]*40, startCoordinates[1]*40))

def addFloor(level, startCoordinates, leftend, length, rightend):
    x = startCoordinates[0]*40
    y = startCoordinates[1]*40
    level.append((leftend, x, y))
    for i in range(length-2):
        x += 40
        level.append((FP, x, y))
    x += 40
    level.append((rightend, x, y))

def addWall(level, startCoordinates, bottomend, height, topend):
    x = startCoordinates[0]*40
    y = startCoordinates[1]*40
    level.append((bottomend, x, y))
    for i in range(height-2):
        y -= 40
        level.append((WP, x, y))
    y -= 40
    level.append((topend, x, y))


level1platforms = []
level2platforms = []

levels = [level1platforms, level2platforms]

addFloor(level1platforms, (65, 8), LFP, 20, RFP)
addSinglePlatform(level1platforms, (12, 9))
addFloor(level1platforms, (0, 11), FP, 17, BRCP)
addWall(level1platforms, (16, 10), WP, 5, TWP)
addFloor(level1platforms, (5, 14), LFP, 20, RFP)
addFloor(level1platforms, (25, 12), LFP, 20, RFP)
addFloor(level1platforms, (45, 10), LFP, 20, RFP)

addFloor(level2platforms, (80, 8), LFP, 20, RFP)
addSinglePlatform(level2platforms, (2, 9))
addFloor(level2platforms, (0, 11), FP, 11, BRCP)
addFloor(level2platforms, (5, 14), LFP, 20, RFP)
addWall(level2platforms, (19, 8), WP, 5, TWP)
addFloor(level2platforms, (25, 12), LFP, 20, RFP)
addFloor(level2platforms, (45, 10), LFP, 20, RFP)
